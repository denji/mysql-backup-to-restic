package helper

import (
	"fmt"
)

func JoinFormatInterfaces(separator string, interfaces ...interface{}) string {
	joined := ""

	for _, iface := range interfaces {
		joined = joined + separator + fmt.Sprintf("%v", iface)
	}

	return joined
}
