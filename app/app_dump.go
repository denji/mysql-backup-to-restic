package app

import (
	"compress/gzip"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	MYSQLDUMP_BINARY = "mysqldump"
)

func (app *App) dumpDatabaseMysqldump(dbName string) string {

	err := os.MkdirAll(app.dumpDir, 0750)
	if err != nil {
		logrus.Fatalf("failed to create dump dir: %v, error: %+v", app.dumpDir, err)
	}

	outPath := filepath.Join(app.dumpDir, dbName+".sql")

	// append extension if gz compression is configured
	if app.config.Dump.CompressWithGz {
		outPath += ".gz"
	}

	logrus.Infof("dumping database: %v to file: %v", dbName, outPath)

	// find binary
	binary, err := exec.LookPath(MYSQLDUMP_BINARY)
	if err != nil {
		logrus.Fatalf("failed to find mysqldump binary: %v, err: %v", MYSQLDUMP_BINARY, err)
	}

	// output file
	outFile, err := os.Create(outPath)
	if err != nil {
		logrus.Fatalf("failed to create output file: %v", err)
	}
	defer outFile.Close()

	// add connection details
	var cmdArgs []string
	cmdArgs = []string{
		"-P" + strconv.Itoa(app.config.MySQL.Port),
		"-h" + app.config.MySQL.Host,
		"-u" + app.config.MySQL.Username,
		"-p" + app.config.MySQL.Password,
	}

	// configure dump options
	if app.config.Dump.SkipLockTables {
		cmdArgs = append(cmdArgs, "--skip-lock-tables");
	}
	if app.config.Dump.SingleTransaction {
		cmdArgs = append(cmdArgs, "--single-transaction");
	}
	if app.config.Dump.Quick {
		cmdArgs = append(cmdArgs, "--quick");
	}

	// add database name to dump
	cmdArgs = append(cmdArgs, dbName);
	logrus.Infof("dumping database with: %v %v",
		binary,
		strings.Replace(
			strings.Join(cmdArgs, " "),
			app.config.MySQL.Password, "<hidden-password>", -1))

	// create command
	cmd := exec.Command(binary, cmdArgs...)

	// use gz compression if configured
	if app.config.Dump.CompressWithGz {
		// create gzWriter
		gzWriter := gzip.NewWriter(outFile)
		defer gzWriter.Flush()
		defer gzWriter.Close()

		// attach compressed writer to gz command
		cmd.Stdout = gzWriter
	} else {
		cmd.Stdout = outFile
	}

	// attach stderr
	stderr, err := cmd.StderrPipe()
	if err != nil {
		logrus.Fatalf("failed to create command: %v", err)
	}

	// execute
	if err := cmd.Start(); err != nil {
		logrus.Fatalf("failed to start command: %v", err)
	}

	// read stderr
	stderrorBytes, err := ioutil.ReadAll(stderr)
	if err != nil {
		logrus.Fatalf("failed to read from stderr: %v", err)
	}

	mysqldumpErrStr := string(stderrorBytes)
	if len(mysqldumpErrStr) > 0 {
		logrus.Fatalf("mysqldump stderr: %v", mysqldumpErrStr)
	}

	return outPath
}
